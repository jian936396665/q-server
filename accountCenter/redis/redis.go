package redis

import (
	"context"
	"github.com/go-redis/redis/v8"
	"time"
)

var (
	redisIp    = "8.140.173.177"
	redisPort  = "7379"
	expireTime = 600
	rdb        *redis.Client
)

func init() {

	if err := initClient(); err != nil {
		panic(err)
	}

}

func initClient() (err error) {

	rdb = redis.NewClient(&redis.Options{
		Addr:     redisIp + ":" + redisPort,
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	_, err = rdb.Ping(ctx).Result()
	if err != nil {
		return err
	}

	return nil
}

func SaveToRedis(key string, code string) bool {

	ctx := context.Background()

	err := rdb.Set(ctx, key, code, time.Duration(expireTime)*time.Second).Err()
	if err != nil {
		return false
	}
	return true

}

func GetFromRedis(key string) string {

	ctx := context.TODO()
	value, err := rdb.Get(ctx, key).Result()
	if err != nil {
		return ""
	}

	return value
}
