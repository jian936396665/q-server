package dao

import (
	"accountCenter/db"
	. "accountCenter/model"
	"accountCenter/proto"
	"github.com/go-basic/uuid"
	"log"
	"time"
)

type AccountDao struct {
}

// Add /*
func (user *AccountDao) Add(entity UserInfoEntity) (userinfo UserInfo, code int, msg string, err error) {

	userinfo = UserInfo{
		UserId:        uuid.New(),
		UserEmail:     entity.UserEmail,
		UserPhoneNum:  entity.UserPhoneNum,
		UserPassword:  entity.UserPassword,
		UserLoginTime: time.Now(),
		CreateTime:    time.Now(),
		UpdateTime:    time.Now(),
		UserEnable:    0,
		UserRole:      "unconfirmed",
		Permission:    "default",
	}

	if db.GormDb.HasTable(&UserInfo{}) {
		db.GormDb.AutoMigrate(&UserInfo{})
	} else {
		db.GormDb.CreateTable(&UserInfo{})
	}

	err = db.GormDb.Create(&userinfo).Error

	if err != nil {
		return userinfo, 51101, "操作失败，请重试", err
	}

	return userinfo, 200, "", err
}

// GetAll /*
func (user *AccountDao) GetAll() (users []UserInfo, code int, msg string, err error) {

	if err := db.GormDb.Find(&users).Error; err != nil {
		log.Println(err)
	}

	return users, 200, "", err
}

// Query /*
func (user *AccountDao) Query(req *proto.QueryUsersRequest) (users []UserInfo, total int, code int, msg string, err error) {

	pageSize := req.GetLimit()
	if pageSize == 0 {
		pageSize = 30
	}
	page := req.GetPage()
	if page < 1 {
		page = 1
	}

	gormDb := db.GormDb.Model(UserInfo{})

	filerUserRole := req.GetFilterUserRole()

	if filerUserRole != "" {
		gormDb = gormDb.Where("user_role = ?", filerUserRole)
	}

	filerUserName := req.GetFilterUserName()

	if filerUserName != "" {
		gormDb = gormDb.Where("user_name = ?", filerUserName)
	}

	if err := gormDb.Count(&total).Error; err != nil {
		log.Println(err)
	}

	if page > 0 && pageSize > 0 {
		gormDb = gormDb.Limit(pageSize).Offset((page - 1) * pageSize)
	}
	if err := gormDb.Find(&users).Error; err != nil {
		log.Println(err)
	}

	return users, total, 20000, "", err
}

// GetOne /*
func (user *AccountDao) GetOne(userId string) (userinfo UserInfo, err error) {

	err = db.GormDb.Where("user_id = ?", userId).First(&userinfo).Error

	if err != nil {
		log.Println(err)
		return userinfo, err
	}

	return userinfo, err
}

// GetOneWithPhoneNumEmail GetOneWithPhoneNum  /*
func (user *AccountDao) GetOneWithPhoneNumEmail(phoneNum string, email string) (userinfo UserInfo, err error) {

	err = db.GormDb.Where("user_phone_num = ?", phoneNum).First(&userinfo).Error

	if userinfo.UserId != "" {
		return userinfo, nil
	}

	err = db.GormDb.Where("user_email = ?", email).First(&userinfo).Error

	if userinfo.UserId != "" {
		return userinfo, nil
	}

	if err != nil {
		log.Println(err)
		return userinfo, err
	}

	return userinfo, nil
}

// Update /*
func (user *AccountDao) Update(entity UserInfoEntity) (userinfo UserInfo, code int, msg string, err error) {

	userinfo, err = user.GetOne(entity.UserId)

	if err != nil {
		log.Println(err)
		return userinfo, 51201, "操作失败，请重试", err
	}

	if entity.Token != "" {
		userinfo.Token = entity.Token
	}
	if entity.UserPassword != "" {
		userinfo.UserPassword = entity.UserPassword
	}
	if entity.UserName != "" {
		userinfo.UserName = entity.UserName
	}
	if entity.UserEmail != "" {
		userinfo.UserEmail = entity.UserEmail
	}
	if entity.UserPhoneNum != "" {
		userinfo.UserPhoneNum = entity.UserPhoneNum
	}
	if entity.Permission != "" {
		userinfo.Permission = entity.Permission
	}
	if entity.UserRole != "" {
		userinfo.UserRole = entity.UserRole
	}
	if entity.UserIntroduction != "" {
		userinfo.UserIntroduction = entity.UserIntroduction
	}
	if entity.UserEnable != -1 {
		userinfo.UserEnable = entity.UserEnable
	}
	if !entity.LoginTime.IsZero() {
		userinfo.UserLoginTime = entity.LoginTime
	}

	userinfo.UpdateTime = time.Now()
	err = db.GormDb.Model(&userinfo).Update(userinfo).Error

	if err != nil {
		log.Println(err)
		return userinfo, 51202, "操作失败，请重试", err
	}

	return userinfo, 20000, "", err
}

// AddNavMenu  /*
func (user *AccountDao) AddNavMenu() (navInfo UserNavInfo, code int, msg string, err error) {

	navInfo = UserNavInfo{
		UserNavId:  uuid.New(),
		CreateTime: time.Now(),
		UpdateTime: time.Now(),
	}

	if db.GormDb.HasTable(&UserNavInfo{}) {
		db.GormDb.AutoMigrate(&UserNavInfo{})
	} else {
		db.GormDb.CreateTable(&UserNavInfo{})
	}

	err = db.GormDb.Create(&navInfo).Error

	if err != nil {
		return navInfo, 51101, "操作失败，请重试", err
	}

	return navInfo, 200, "", err
}
