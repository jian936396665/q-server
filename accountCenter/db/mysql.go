package db

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var GormDb *gorm.DB

func init() {

	//connStr := fmt.Sprintf(
	//	"%s:%s@tcp(%s:%d)/local?parseTime=true&loc=Local",
	//	config.DBCfg.MySQL.User,
	//	config.DBCfg.MySQL.Password,
	//	config.DBCfg.MySQL.Host,
	//	config.DBCfg.MySQL.Port,
	//)
	connStr := "root:R00tAdmin@tcp(8.140.173.110:3306)/local?parseTime=true&loc=Local"
	var err error
	GormDb, err = gorm.Open("mysql", connStr)

	if err != nil {
		panic(err)
	}
	//设置全局表名禁用复数
	GormDb.SingularTable(false)
}
