package verification_code

import (
	. "accountCenter/redis"
	. "accountCenter/utils"
	"fmt"
	openapi "github.com/alibabacloud-go/darabonba-openapi/client"
	dysmsapi20170525 "github.com/alibabacloud-go/dysmsapi-20170525/v2/client"
	"github.com/alibabacloud-go/tea/tea"
)

var (
	accessSecret = "mhxKO5INrRoq7NBunkQVdEUeuC3ZSI"
	accesskeyId  = "LTAI5tGQVhFG9MeUNRGZz3pZ"
)

func CreateClient(accessKeyId *string, accessKeySecret *string) (_result *dysmsapi20170525.Client, _err error) {
	config := &openapi.Config{
		// 您的AccessKey ID
		AccessKeyId: accessKeyId,
		// 您的AccessKey Secret
		AccessKeySecret: accessKeySecret,
	}
	// 访问的域名
	config.Endpoint = tea.String("dysmsapi.aliyuncs.com")
	_result = &dysmsapi20170525.Client{}
	_result, _err = dysmsapi20170525.NewClient(config)
	return _result, _err
}

// SendSms 向手机发送验证码
func SendSms(tel string, code string) string {

	client, _err := CreateClient(tea.String(accesskeyId), tea.String(accessSecret))
	if _err != nil {
		return "failed"
	}

	sendSmsRequest := &dysmsapi20170525.SendSmsRequest{
		PhoneNumbers:  tea.String(tel),
		SignName:      tea.String("指悦互娱"),
		TemplateCode:  tea.String("SMS_226275316"),
		TemplateParam: tea.String("{\"code\":\"" + code + "\"}"),
	}

	_, _err = client.SendSms(sendSmsRequest)

	if _err != nil {
		fmt.Print(_err.Error())
		return "failed"
	}
	return "success"
}

// GetSmsValidation 接收手机号并发送验证码
func GetSmsValidation(tel string) (string, error) {
	code := RandomCode()
	sendRes := SendSms(tel, code)
	if sendRes == "success" {
		SaveToRedis("phone"+tel, code)
	}
	return code, nil
}

func ValidationSmsCode(userId string, validation string) bool {
	var flag bool
	code := GetFromRedis("phone" + userId)

	if validation == code {
		flag = true
	} else {
		flag = false
	}
	return flag
}
