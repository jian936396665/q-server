package handler

import (
	. "accountCenter/dao"
	. "accountCenter/model"
	proto "accountCenter/proto"
	. "accountCenter/verification-code"
	"context"
	"strconv"
	"time"
)

type AccountCenterHandler struct{}

func (account *AccountCenterHandler) CreatUser(ctx context.Context, req *proto.CreateUserRequest, rsp *proto.CreateUserResponse) error {

	user := AccountDao{}

	userPhone := req.PhoneNum
	userEmail := req.Email

	if !ValidationSmsCode(req.PhoneNum, req.SmsCode) {
		rsp.Code = 3000
		rsp.Msg = "短信验证码不正确"
		return nil
	}

	dbUser, _ := user.GetOneWithPhoneNumEmail(userPhone, userEmail)

	if dbUser.UserId != "" {
		rsp.Code = 3000
		rsp.Msg = "用户已存在，注册失败"
		return nil
	}

	userinfo, code, _, err := user.Add(UserInfoEntity{
		UserPhoneNum: req.PhoneNum,
		UserEmail:    req.Email,
		UserPassword: req.Password,
	})
	rsp.UserId = userinfo.UserId
	rsp.UserName = userinfo.UserPhoneNum
	rsp.CreateTime = userinfo.CreateTime.Unix()
	rsp.UpdateTime = userinfo.UpdateTime.Unix()
	rsp.Code = int64(code)
	rsp.Msg = "注册成功"
	return err

}

func (account *AccountCenterHandler) LoginWithSms(ctx context.Context, req *proto.LoginRequest, rsp *proto.LoginResponse) error {
	user := AccountDao{}
	userinfo, err := user.GetOneWithPhoneNumEmail(req.PhoneNum, req.Email)
	if err != nil {
		rsp.Code = 4001
		rsp.Msg = "登录失败，账户不存在"
		rsp.Success = false
		return nil
	}

	if req.PhoneNum != "" && req.SmsCode != 0 {

		if req.PhoneNum == userinfo.UserPhoneNum && ValidationSmsCode(req.PhoneNum, strconv.FormatInt(req.SmsCode, 10)) {
			rsp.UserId = userinfo.UserId
			rsp.Permission = userinfo.Permission
			rsp.Success = true
			rsp.Code = 200
			rsp.Msg = "登录成功"

			user.Update(UserInfoEntity{
				UserId:    userinfo.UserId,
				LoginTime: time.Now(),
			})

		} else {
			rsp.Success = false
			rsp.Code = 4000
			rsp.Msg = "登录失败，验证码错误"
		}

	}
	return nil
}

func (account *AccountCenterHandler) Login(ctx context.Context, req *proto.LoginRequest, rsp *proto.LoginResponse) error {
	user := AccountDao{}
	userinfo, err := user.GetOneWithPhoneNumEmail(req.PhoneNum, req.Email)
	if err != nil {
		rsp.Code = 4001
		rsp.Msg = "登录失败，账户不存在"
		rsp.Success = false
		return nil
	}

	if req.PhoneNum != "" {

		if req.PhoneNum == userinfo.UserPhoneNum && req.Password == userinfo.UserPassword {
			rsp.UserId = userinfo.UserId
			rsp.Permission = userinfo.Permission
			rsp.Success = true
			rsp.Code = 200
			rsp.Msg = "登录成功"

			user.Update(UserInfoEntity{
				UserId:    userinfo.UserId,
				LoginTime: time.Now(),
			})

		} else {
			rsp.Success = false
			rsp.Code = 4000
			rsp.Msg = "登录失败，账户密码错误"
		}

	} else {

		if req.Email == userinfo.UserEmail && req.Password == userinfo.UserPassword {
			rsp.UserId = userinfo.UserId
			rsp.Permission = userinfo.Permission
			rsp.Success = true
			rsp.Code = 200
			rsp.Msg = "登录成功"

			user.Update(UserInfoEntity{
				UserId:    userinfo.UserId,
				LoginTime: time.Now(),
			})
		} else {
			rsp.Success = false
			rsp.Code = 4000
			rsp.Msg = "登录失败，账户密码错误"
		}

	}

	return nil

}

func (account *AccountCenterHandler) UpdateUser(ctx context.Context, req *proto.UpdateUserRequest, rsp *proto.UpdateUserResponse) error {

	user := AccountDao{}

	userInfo, err := user.GetOne(req.UserId)
	if err != nil {
		rsp.Code = 4001
		rsp.Msg = "信息更新失败，账户不存在"
		rsp.Success = false
		return nil
	}

	var updatePwd string
	updatePwd = userInfo.UserPassword

	if req.UpdateType == int64(proto.ModifyType_UserBaseInfo) {
		updatePwd = req.UserPassword
	} else if req.UpdateType == int64(proto.ModifyType_UserPassWord) {
		updatePwd = req.NewUserPassword
		if req.UserPassword != userInfo.UserPassword {
			rsp.Code = 4001
			rsp.Msg = "修改失败，密码不正确"
			rsp.Success = false
			return nil
		}
	} else if req.UpdateType == int64(proto.ModifyType_UserEmail) {

		if req.UserPassword != userInfo.UserPassword {
			rsp.Code = 4001
			rsp.Msg = "修改失败，密码不正确"
			rsp.Success = false
			return nil
		}

		if !ValidationEmailCode(req.UserEmail, req.UserVCode) {
			rsp.Code = 4001
			rsp.Msg = "信息更新失败,邮箱验证码不正确"
			return nil
		}

	} else if req.UpdateType == int64(proto.ModifyType_UserPhoneNum) {

		if req.UserPassword != userInfo.UserPassword {
			rsp.Code = 4001
			rsp.Msg = "修改失败，密码不正确"
			rsp.Success = false
			return nil
		}
		if !ValidationSmsCode(req.UserPhoneNum, req.UserVCode) {
			rsp.Code = 4001
			rsp.Msg = "信息更新失败,手机验证码不正确"
			return nil
		}

	} else if req.UpdateType == int64(proto.ModifyType_UserResetPwd) {
		updatePwd = req.NewUserPassword
	}

	_, _, _, error := user.Update(UserInfoEntity{
		UserId:           req.UserId,
		Token:            req.Token,
		UserPhoneNum:     req.UserPhoneNum,
		UserEmail:        req.UserEmail,
		UserPassword:     updatePwd,
		UserName:         req.UserName,
		Permission:       req.Permission,
		UserIntroduction: req.UserIntroduction,
		UserRole:         req.UserRole,
		UserEnable:       int(req.UserEnable),
	})

	rsp.Code = 200
	rsp.Msg = "信息更新成功"
	rsp.Success = true

	return error
}

func (account *AccountCenterHandler) QueryUserDetail(ctx context.Context, req *proto.QueryUserDetailRequest, rsp *proto.QueryUserDetailResponse) error {

	user := AccountDao{}

	userinfoId, IDError := user.GetOne(req.UserId)

	userinfoEmailPhone, EmailPhoneError := user.GetOneWithPhoneNumEmail(req.UserPhoneNum, req.UserEmail)
	var userinfo UserInfo
	timeLayout := "2006-01-02 15:04:05"

	if userinfoId.UserId != "" {
		userinfo = userinfoId
		rsp.UserInfo = &proto.QueryUserInfoResponse{
			UserId:           userinfo.UserId,
			UserName:         userinfo.UserName,
			UserEmail:        userinfo.UserEmail,
			UserPhoneNum:     userinfo.UserPhoneNum,
			UserIntroduction: userinfo.UserIntroduction,
			UserRole:         userinfo.UserRole,
			UserAvatar:       userinfo.UserAvatar,
			UserEnable:       int64(userinfo.UserEnable),
			Permission:       userinfo.Permission,
			LoginTime:        userinfo.UserLoginTime.Format(timeLayout),
		}
		rsp.Success = true
		rsp.Code = 200
		rsp.Msg = ""
		return IDError
	}

	if userinfoEmailPhone.UserId != "" {
		userinfo = userinfoEmailPhone
		rsp.UserInfo = &proto.QueryUserInfoResponse{
			UserId:           userinfo.UserId,
			UserName:         userinfo.UserName,
			UserEmail:        userinfo.UserEmail,
			UserPhoneNum:     userinfo.UserPhoneNum,
			UserIntroduction: userinfo.UserIntroduction,
			UserRole:         userinfo.UserRole,
			UserAvatar:       userinfo.UserAvatar,
			UserEnable:       int64(userinfo.UserEnable),
			Permission:       userinfo.Permission,
			LoginTime:        userinfo.UserLoginTime.Format(timeLayout),
		}
		rsp.Success = true
		rsp.Code = 200
		rsp.Msg = ""
		return EmailPhoneError
	}

	rsp.Code = 4001
	rsp.Msg = "获取信息更新失败，账户不存在"
	rsp.Success = false
	return nil
}

func (account *AccountCenterHandler) QueryUsers(ctx context.Context, req *proto.QueryUsersRequest, rsp *proto.QueryUsersResponse) error {

	user := AccountDao{}
	users, total, _, _, err := user.Query(req)

	var userList []*proto.QueryUserInfoResponse
	timeLayout := "2006-01-02 15:04:05"
	for _, value := range users {
		var user proto.QueryUserInfoResponse
		user.UserId = value.UserId
		user.UserEmail = value.UserEmail
		user.UserPhoneNum = value.UserPhoneNum
		user.UserName = value.UserName
		user.UserRole = value.UserRole
		user.UserIntroduction = value.UserIntroduction
		user.CreateTime = value.CreateTime.Format(timeLayout)
		user.LoginTime = value.UserLoginTime.Format(timeLayout)
		userList = append(userList, &user)

	}
	rsp.Total = int64(total)
	rsp.Users = userList

	return err
}

func (account *AccountCenterHandler) VerifyUserSendCode(ctx context.Context, req *proto.VerifyCodeRequest, rsp *proto.VerifyCodeResponse) error {

	if ValidationEmailCode(req.Email, req.VCode) {

		rsp.Code = 200
		rsp.Success = true
		return nil
	}

	if ValidationSmsCode(req.PhoneNum, req.VCode) {

		rsp.Code = 200
		rsp.Success = true
		return nil
	}
	rsp.Code = 4001
	rsp.Msg = "验证码不争取"
	rsp.Success = false
	return nil

}

func (account *AccountCenterHandler) UserSendCode(ctx context.Context, req *proto.SendCodeRequest, rsp *proto.SendCodeResponse) error {

	if req.Type == "sms" {
		vCode, err := GetSmsValidation(req.PhoneNum)
		rsp.CodeValue = vCode
		if err != nil {
			return err
		}
	} else {
		vCode, err := GetEmailValidation(req.Email)
		rsp.CodeValue = vCode
		if err != nil {
			return err
		}
	}
	return nil

}

func (account *AccountCenterHandler) CreatUserRole(ctx context.Context, req *proto.CreateUserRoleRequest, rsp *proto.CreateUserRoleResponse) error {

	role := UserRoleDao{}

	roleInfo, code, _, err := role.AddUserRole(UserRoleEntity{
		RoleNum:    req.RoleNum,
		RoleIdName: req.RoleIdName,
		RoleName:   req.RoleName,
		RoleDes:    req.RoleDes,
		RoleEnable: int(req.RoleEnable),
	})
	rsp.RoleId = roleInfo.RoleId
	rsp.RoleNum = roleInfo.RoleNum
	rsp.RoleIdName = roleInfo.RoleIdName
	rsp.RoleDes = roleInfo.RoleDes
	rsp.RoleEnable = int64(roleInfo.RoleEnable)
	rsp.RoleName = roleInfo.RoleName
	rsp.CreateTime = roleInfo.CreateTime.Unix()
	rsp.UpdateTime = roleInfo.UpdateTime.Unix()
	rsp.Code = int64(code)
	rsp.Msg = "创建成功"
	return err
}

func (account *AccountCenterHandler) QueryRoles(ctx context.Context, req *proto.QueryRolesRequest, rsp *proto.QueryRolesResponse) error {

	role := UserRoleDao{}

	roles, _, _, err := role.GetAll()

	var roleList []*proto.QueryRoleInfoResponse

	for _, value := range roles {
		var role proto.QueryRoleInfoResponse
		role.RoleId = value.RoleId
		role.RoleIdName = value.RoleIdName
		role.RoleName = value.RoleName
		role.RoleNum = value.RoleNum
		role.RoleDes = value.RoleDes
		role.CreateTime = value.CreateTime.Unix()
		roleList = append(roleList, &role)
	}
	rsp.Total = int64(len(roleList))
	rsp.Roles = roleList

	return err
}

func (account *AccountCenterHandler) UpdateRole(ctx context.Context, req *proto.UpdateRoleRequest, rsp *proto.UpdateRoleResponse) error {

	role := UserRoleDao{}

	_, err := role.GetOne(req.RoleId)
	if err != nil {
		rsp.Code = 4001
		rsp.Msg = "信息更新失败，信息不存在"
		rsp.Success = false
		return nil
	}

	_, _, _, error := role.Update(UserRoleInfo{
		RoleId:     req.RoleId,
		RoleName:   req.RoleName,
		RoleIdName: req.RoleIdName,
		RoleDes:    req.RoleDes,
		RoleEnable: int(req.RoleEnable),
	})

	rsp.Code = 200
	rsp.Msg = "信息更新成功"
	rsp.Success = true

	return error
}
