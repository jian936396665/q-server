package config

import "github.com/timest/env"

type DBConfig struct {
	MySQL struct {
		Host     string
		Port     int
		User     string
		Password string
	}
}

var DBCfg *DBConfig

func init() {
	DBCfg = new(DBConfig)
	env.IgnorePrefix()
	err := env.Fill(DBCfg)
	if err != nil {
		panic(err)
	}
}
