package main

import (
	handler "accountCenter/handler"
	proto "accountCenter/proto"
	_ "accountCenter/redis"
	_ "github.com/asim/go-micro/plugins/registry/kubernetes/v3"
	"github.com/asim/go-micro/v3"
	"github.com/asim/go-micro/v3/logger"
)

const (
	ServiceName = "Account-Center-Service"
)

func main() {

	service := micro.NewService(
		micro.Name(ServiceName),
		micro.Version("latest"),
		micro.Metadata(map[string]string{
			"type": "AccountCenterService",
		}),
	)

	service.Init()

	if err := proto.RegisterUserServiceHandler(service.Server(), new(handler.AccountCenterHandler)); err != nil {
		logger.Fatal(err)
	}

	if err := service.Run(); err != nil {
		logger.Fatal(err)
	}
}
