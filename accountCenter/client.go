package main

import (
	proto "accountCenter/proto"
	"context"
	"github.com/asim/go-micro/v3"
	"github.com/asim/go-micro/v3/logger"
)

const (
	TServiceName = "AccountCenterService"
)

func main() {

	service := micro.NewService()

	service.Init()

	client := proto.NewUserService(TServiceName,service.Client())

	rsp, err := client.CreatUser(context.Background(),&proto.CreateUserRequest{
		Email: "test@gmail.com",
		PhoneNum: "18600031398",
		Password: "1235",
	})

	if err != nil {
		logger.Info(err)
		return
	}
	logger.Info(rsp)

}