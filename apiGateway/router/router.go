package router

import (
	"apiGateway/handler"
	md "apiGateway/middleware"
	"net/http"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func InitRouter() *gin.Engine {
	gin.SetMode(gin.ReleaseMode)
	//router := gin.Default()

	//test
	router := gin.New()
	//test end

	router.Use(Cors())

	authApi := router.Group("/auth/v1")

	// @Description 用户登录
	authApi.POST("/login", handler.UserLoginHandler)
	// @Description 注册用户
	authApi.POST("/register", handler.UserRegisterHandler)
	// @Description 用户身份验证
	authApi.POST("/verifyUserInfo", handler.VerifyUserInfoHandler)
	// @Description 重置密码
	authApi.POST("/resetPassWord", handler.ResetUserPwdHandler)
	// @Description 获取短信验证码
	authApi.GET("/smsCode", handler.UserSmsCodeHandler)
	// @Description 获取邮箱验证码
	authApi.GET("/emailCode", handler.UserEmailCodeHandler)

	userApi := router.Group("/user/v1")

	// @Description 用户列表
	userApi.GET("/userList", handler.UserListHandler)
	// @Description 修改用户信息
	userApi.POST("/updateUserInfo", handler.UpdateUserInfoHandler)
	// @Description 获取用户信息
	userApi.GET("/userInfo", handler.UserInfoHandler)

	userApi.GET("/userNav", handler.UserNavHandler)
	// @Description 新增角色
	userApi.POST("/userRole/createRole", handler.UserRoleCreateHandler)
	// @Description 修改角色信息
	userApi.POST("/userRole/updateRoleRole", handler.UpdateRoleInfoHandler)
	// @Description 角色列表
	userApi.GET("/userRole/roleList", handler.RoleListHandler)

	dispatchApi := router.Group("/dispatch/v1")

	// @Description 创建手工派单
	dispatchApi.POST("/addTask", handler.DispatchTaskHandler)
	// @Description 待派单列表
	dispatchApi.GET("/waitList", handler.WaitDispatchListHandler)

	reviewApi := router.Group("/review/v1")

	// @Description 待审核列表
	reviewApi.GET("/waitReview", handler.GetUnReviewListHandler)

	// @Description 快照回滚&&快照二次更新
	reviewApi.POST("/SubmitSnapShot", handler.SubmitReviewSnapShotHandler)

	taskApi := router.Group("/audio/v1")

	// @Description 总转换时长
	taskApi.GET("/convertTotalTime", handler.GetTotalTranslateLengthHandler)

	// @Description 创建转换任务
	taskApi.POST("/createConvert", handler.CreateConvertHandler)
	// @Description 删除转换任务
	taskApi.POST("/deleteConvert", handler.DeleteConvertHandler)
	// @Description 待转换任务列表
	taskApi.GET("/convertList", handler.GetConvertListHandler)
	// @Description 查询任务详情
	taskApi.GET("/convertDetail", handler.GetConvertDetailHandler)
	// @Description 提交暂存
	taskApi.POST("/convertSubmitSnapShot", handler.SubmitConvertSnapShotHandler)
	// @Description 已审核快照列表
	taskApi.GET("/convertSnapshotList", handler.GetConvertSnapshotListHandler)
	// @Description 审核快照详情
	taskApi.GET("/convertSnapshotDetail", handler.GetConvertSnapshotDetailHandler)

	v1Api := router.Group("/api/v1")
	v1Api.Use(md.JWTAuth())
	{
		v1Api.POST("/jwt", handler.UserRegisterHandler)
	}

	url := ginSwagger.URL("http://localhost:8090/swagger/doc.json")
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return router
}

func Cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		method := c.Request.Method
		origin := c.Request.Header.Get("Origin")
		if origin != "" {
			c.Header("Access-Control-Allow-Origin", origin)
			c.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, UPDATE")
			c.Header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Token, Access-Token")
			c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Cache-Control, Content-Language, Content-Type")
			c.Header("Access-Control-Allow-Credentials", "true")
			c.Set("content-type", "application/json")
		}
		if method == "OPTIONS" {
			c.AbortWithStatus(http.StatusNoContent)
		}
		c.Next()
	}
}
