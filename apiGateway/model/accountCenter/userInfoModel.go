package accountCenter

type UserInfoResponseEntity struct {
	UserName         string `json:"userName"`
	UserEmail        string `json:"userEmail"`
	UserPhoneNum     string `json:"userPhoneNum"`
	UserPassword     string `json:"userPassword"`
	SmsCode          int    `json:"smsCode"`
	Token            string `json:"token"`
	UserId           string `json:"userId"`
	LoginMode        string `json:"loginMode"`
	LoginTime        string `json:"loginTime"`
	Permission       string `json:"permission"`
	UserIntroduction string `json:"userIntroduction"`
	UserRole         string `json:"userRole"`
	UserEnable       int    `json:"userEnable"`
	UserAvatar       string `json:"userAvatar"`
	CreatedAt        string `json:"createdAt"`
}

type BaseResponseEntity struct {
	Code int64       `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

type RegisterResponseEntity struct {
	Code int64                  `json:"code"`
	Msg  string                 `json:"msg"`
	Data UserInfoResponseEntity `json:"data"`
}

type LoginResponseEntity struct {
	Code int64                  `json:"code"`
	Msg  string                 `json:"msg"`
	Data UserInfoResponseEntity `json:"data"`
}

type UserListResponseEntity struct {
	Code int64                    `json:"code"`
	Msg  string                   `json:"msg"`
	Data UserDataListResultEntity `json:"data"`
}

type UserDataListResultEntity struct {
	Total int64                    `json:"total"`
	Rows  []UserInfoResponseEntity `json:"rows"`
}

type QueryUserInfoResponseEntity struct {
	Code int64                  `json:"code"`
	Msg  string                 `json:"msg"`
	Data UserInfoResponseEntity `json:"data"`
}

type QueryUserNavResponseEntity struct {
	Code int64               `json:"code"`
	Msg  string              `json:"msg"`
	Data []UserNavInfoEntity `json:"data"`
}

type UserNavInfoEntity struct {
	Name      string          `json:"name"`
	ParentId  int64           `json:"parentId"`
	Id        int64           `json:"id"`
	Meta      UserNavMetaInfo `json:"meta"`
	Component string          `json:"component"`
	Redirect  string          `json:"redirect"`
	Path      string          `json:"path"`
}
type UserNavMetaInfo struct {
	Icon  string `json:"icon"`
	Title string `json:"title"`
	Show  bool   `json:"show"`
}

type VerificationCodeResponseEntity struct {
	Code int64                  `json:"code"`
	Msg  string                 `json:"msg"`
	Data VerificationCodeEntity `json:"data"`
}

type VerificationCodeEntity struct {
	VCode string `json:"vCode"`
}
