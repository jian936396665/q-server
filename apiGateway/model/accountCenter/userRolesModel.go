package accountCenter

import "time"

type UserRoleEntity struct {
	RoleNum    string
	RoleName   string
	RoleDes    string
	RoleEnable int
}

type UserRoleInfo struct {
	RoleId     string `gorm:"primary_key"`
	RoleNum    string
	RoleName   string
	RoleDes    string
	RoleEnable int
	CreateTime time.Time
	UpdateTime time.Time
}

type RoleListRowDetailEntity struct {
	RoleId       string `json:"roleId"`
	RoleIdName   string `json:"roleIdName"`
	RoleName     string `json:"roleName"`
	RoleCreateAt string `json:"roleCreateAt"`
	RoleNum      string `json:"roleNum"`
	RoleDes      string `json:"roleDes"`
	RoleEnable   int64  `json:"roleEnable"`
}

type RoleListResponseEntity struct {
	Code int64                    `json:"code"`
	Msg  string                   `json:"msg"`
	Data RoleDataListResultEntity `json:"data"`
}

type RoleDataListResultEntity struct {
	Total int64                     `json:"total"`
	Rows  []RoleListRowDetailEntity `json:"rows"`
}
