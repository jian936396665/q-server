package taskService

type TranslateDetailResponseEntity struct {
	Code int64                 `json:"code"`
	Msg  string                `json:"msg"`
	Data TranslateDetailEntity `json:"data"`
}

type TranslateDetailEntity struct {
	AudioId              string                  `json:"audioId"`
	AudioName            string                  `json:"audioName"`
	AudioUri             string                  `json:"audioUri"`
	AudioCreatedAt       int64                   `json:"audioCreatedAt"`
	TranslateJoinAt      int64                   `json:"translateJoinAt"`
	TextCount            int64                   `json:"textCount"`
	TextNum              int64                   `json:"textNum"`
	TranslateTaskId      string                  `json:"translateTaskId"`
	AudioLength          int64                   `json:"audioLength"`
	AudioLengthDes       string                  `json:"audioLengthDes"`
	TranslateResult      []TranslateResultEntity `json:"translateResult"`
	TranslateState       int64                   `json:"translateState"`
	TranslateCompletedAt int64                   `json:"translateCompletedAt"`
}

type TranslateResultEntity struct {
	Sentences string `json:"sentences"`
	StartTime int64  `json:"starttime"`
	EndTime   int64  `json:"endtime"`
	Duration  string `json:"duration"`
	Key       int64  `json:"key"`
}

type AudioTranslateCreateResultEntity struct {
	AudioName         string `json:"audioName"`
	AudioId           string `json:"audioId"`
	TranslatePriority int64  `json:"translatePriority"`
}

type AudioTranslateCreateResponseEntity struct {
	Code int64                            `json:"code"`
	Msg  string                           `json:"msg"`
	Data AudioTranslateCreateResultEntity `json:"data"`
}

type AudioTranslateDeleteResultEntity struct {
}

type AudioTranslateDeleteResponseEntity struct {
	Code int64                            `json:"code"`
	Msg  string                           `json:"msg"`
	Data AudioTranslateDeleteResultEntity `json:"data"`
}

type AudioTranslateResultListResponseEntity struct {
	Code int64                   `json:"code"`
	Msg  string                  `json:"msg"`
	Data []TranslateResultEntity `json:"data"`
}

type AudioUpdateSnapShotEntity struct {
	SnapShotId string `json:"snapShotId"`
}

type AudioUpdateSnapShotResponseEntity struct {
	Code int64                     `json:"code"`
	Msg  string                    `json:"msg"`
	Data AudioUpdateSnapShotEntity `json:"data"`
}

type CovertListResultEntity struct {
	Total int64                       `json:"total"`
	Limit int64                       `json:"limit"`
	Page  int64                       `json:"page"`
	Rows  []CovertListRowDetailEntity `json:"rows"`
}

type CovertListRowDetailEntity struct {
	AudioId              string `json:"audioId"`
	AudioName            string `json:"audioName"`
	AudioLength          string `json:"audioLength"`
	AudioCreatedAt       string `json:"audioCreatedAt"`
	AudioCreatedBy       string `json:"audioCreatedBy"`
	AudioState           int64  `json:"audioState"`
	TranslateJoinAt      string `json:"translateJoinAt"`
	TranslateJoinBy      string `json:"translateJoinBy"`
	DispatchAt           string `json:"dispatchAt"`
	DispatchBy           string `json:"dispatchBy"`
	ReleaseState         bool   `json:"releaseState"`
	ReleaseAt            string `json:"releaseAt"`
	ReviewAt             string `json:"reviewAt"`
	TranslateCompletedAt string `json:"translateCompletedAt"`
	TranslateState       int64  `json:"translateState"`
	AudioTotalWords      int64  `json:"audioTotalWords"`
	ReviewState          int64  `json:"reviewState"`
	ReviewTime           string `json:"reviewTime"`
}

type AudioCovertListResponseEntity struct {
	Code int64                  `json:"code"`
	Msg  string                 `json:"msg"`
	Data CovertListResultEntity `json:"data"`
}

type SnapshotListResultEntity struct {
	Total int64                     `json:"total"`
	Limit int64                     `json:"limit"`
	Page  int64                     `json:"page"`
	Rows  []SnapshotRowDetailEntity `json:"rows"`
}

type SnapshotRowDetailEntity struct {
	SnapshotId        string `json:"snapshotId"`
	SnapshotAt        string `json:"snapshotAt"`
	SnapshotBy        string `json:"snapshotBy"`
	SnapshotDiffRows  int64  `json:"snapshotDiffRows"`
	SnapshotIsRelease int64  `json:"snapshotIsRelease"`
	AudioId           string `json:"audioId"`
	AudioName         string `json:"audioName"`
}

type SnapshotListResponseEntity struct {
	Code int64                    `json:"code"`
	Msg  string                   `json:"msg"`
	Data SnapshotListResultEntity `json:"data"`
}

type SnapshotDetailResponseEntity struct {
	Code int64                `json:"code"`
	Msg  string               `json:"msg"`
	Data SnapshotDetailEntity `json:"data"`
}

type SnapshotDetailEntity struct {
	SnapshotId         string                     `json:"snapshotId"`
	SnapshotAt         string                     `json:"snapshotAt"`
	SnapshotBy         string                     `json:"snapshotBy"`
	SnapshotIsRelease  int64                      `json:"snapshotIsRelease"`
	SnapshotDiffRows   int64                      `json:"snapshotDiffRows"`
	SnapshotDiffResult []SnapshotDiffResultEntity `json:"snapshotDiffResult"`
	AudioId            string                     `json:"audioId"`
	AudioName          string                     `json:"audioName"`
	AudioLength        string                     `json:"audioLength"`
	AudioCreatedAt     string                     `json:"audioCreatedAt"`
	AudioCreatedBy     string                     `json:"audioCreatedBy"`
	AudioState         int64                      `json:"audioState"`
	AudioTotalWords    int64                      `json:"audioTotalWords"`
}

type SnapshotDiffResultEntity struct {
	Key          int64  `json:"key"`
	OldSentences string `json:"oldSentences"`
	NewSentences string `json:"newSentences"`
	Duration     string `json:"duration"`
}

type DispatchCreateDetailEntity struct {
}

type DispatchCreateResponseEntity struct {
	Code int64                      `json:"code"`
	Msg  string                     `json:"msg"`
	Data DispatchCreateDetailEntity `json:"data"`
}
