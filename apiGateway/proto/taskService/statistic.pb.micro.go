// Code generated by protoc-gen-micro. DO NOT EDIT.
// source: proto/taskService/statistic.proto

package taskService

import (
	fmt "fmt"
	proto "google.golang.org/protobuf/proto"
	math "math"
)

import (
	context "context"
	api "github.com/asim/go-micro/v3/api"
	client "github.com/asim/go-micro/v3/client"
	server "github.com/asim/go-micro/v3/server"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// Reference imports to suppress errors if they are not otherwise used.
var _ api.Endpoint
var _ context.Context
var _ client.Option
var _ server.Option

// Api Endpoints for StatisticService service

func NewStatisticServiceEndpoints() []*api.Endpoint {
	return []*api.Endpoint{}
}

// Client API for StatisticService service

type StatisticService interface {
	QuantityUserAudited(ctx context.Context, in *StatisticUserIdRequest, opts ...client.CallOption) (*StatisticUserIdValueRequest, error)
	QuantityUserAuditing(ctx context.Context, in *StatisticUserIdRequest, opts ...client.CallOption) (*StatisticUserIdValueRequest, error)
	PlatformTotalTranslateAudioLength(ctx context.Context, in *StatisticEmptyRequest, opts ...client.CallOption) (*StatisticIntValueRequest, error)
}

type statisticService struct {
	c    client.Client
	name string
}

func NewStatisticService(name string, c client.Client) StatisticService {
	return &statisticService{
		c:    c,
		name: name,
	}
}

func (c *statisticService) QuantityUserAudited(ctx context.Context, in *StatisticUserIdRequest, opts ...client.CallOption) (*StatisticUserIdValueRequest, error) {
	req := c.c.NewRequest(c.name, "StatisticService.QuantityUserAudited", in)
	out := new(StatisticUserIdValueRequest)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *statisticService) QuantityUserAuditing(ctx context.Context, in *StatisticUserIdRequest, opts ...client.CallOption) (*StatisticUserIdValueRequest, error) {
	req := c.c.NewRequest(c.name, "StatisticService.QuantityUserAuditing", in)
	out := new(StatisticUserIdValueRequest)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *statisticService) PlatformTotalTranslateAudioLength(ctx context.Context, in *StatisticEmptyRequest, opts ...client.CallOption) (*StatisticIntValueRequest, error) {
	req := c.c.NewRequest(c.name, "StatisticService.PlatformTotalTranslateAudioLength", in)
	out := new(StatisticIntValueRequest)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for StatisticService service

type StatisticServiceHandler interface {
	QuantityUserAudited(context.Context, *StatisticUserIdRequest, *StatisticUserIdValueRequest) error
	QuantityUserAuditing(context.Context, *StatisticUserIdRequest, *StatisticUserIdValueRequest) error
	PlatformTotalTranslateAudioLength(context.Context, *StatisticEmptyRequest, *StatisticIntValueRequest) error
}

func RegisterStatisticServiceHandler(s server.Server, hdlr StatisticServiceHandler, opts ...server.HandlerOption) error {
	type statisticService interface {
		QuantityUserAudited(ctx context.Context, in *StatisticUserIdRequest, out *StatisticUserIdValueRequest) error
		QuantityUserAuditing(ctx context.Context, in *StatisticUserIdRequest, out *StatisticUserIdValueRequest) error
		PlatformTotalTranslateAudioLength(ctx context.Context, in *StatisticEmptyRequest, out *StatisticIntValueRequest) error
	}
	type StatisticService struct {
		statisticService
	}
	h := &statisticServiceHandler{hdlr}
	return s.Handle(s.NewHandler(&StatisticService{h}, opts...))
}

type statisticServiceHandler struct {
	StatisticServiceHandler
}

func (h *statisticServiceHandler) QuantityUserAudited(ctx context.Context, in *StatisticUserIdRequest, out *StatisticUserIdValueRequest) error {
	return h.StatisticServiceHandler.QuantityUserAudited(ctx, in, out)
}

func (h *statisticServiceHandler) QuantityUserAuditing(ctx context.Context, in *StatisticUserIdRequest, out *StatisticUserIdValueRequest) error {
	return h.StatisticServiceHandler.QuantityUserAuditing(ctx, in, out)
}

func (h *statisticServiceHandler) PlatformTotalTranslateAudioLength(ctx context.Context, in *StatisticEmptyRequest, out *StatisticIntValueRequest) error {
	return h.StatisticServiceHandler.PlatformTotalTranslateAudioLength(ctx, in, out)
}
