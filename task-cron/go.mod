module task-cron

go 1.16

require (
	github.com/asim/go-micro/plugins/registry/kubernetes/v3 v3.0.0-20211006165514-a99a1e935651
	github.com/asim/go-micro/v3 v3.6.0
	google.golang.org/protobuf v1.26.0
//task-service v0.0.0-00010101000000-000000000000
)

//replace task-service => ./../task-service
