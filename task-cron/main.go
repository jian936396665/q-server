package main

import (
	"context"
	_ "github.com/asim/go-micro/plugins/registry/kubernetes/v3"
	"github.com/asim/go-micro/v3/client"
	pb "task-cron/proto"
	"time"

	"github.com/asim/go-micro/v3"
	log "github.com/asim/go-micro/v3/logger"
)

func Query(AudioClient pb.AudioService, TranslateClient pb.TranslateService) error {
	Limit := int64(100)
	SortField := "translate_join_at"
	SortMode := int64(1)
	TranslateState := int64(pb.State_TranslateJoined)
	rsp, err := AudioClient.Query(context.Background(), &pb.QueryRequest{
		Limit:     &Limit,
		SortField: &SortField,
		SortMode:  &SortMode,
		FilterJoinAt: &pb.TimestampRange{
			Start: 0,
			End:   time.Now().Unix() - (5 * 60),
		},
		FilterTranslateState: &TranslateState,
	})
	if err != nil {
		log.Errorf("Query error: %v", err)
		return err
	}

	log.Infof("Query total: %v", rsp.GetTotal())
	if rsp.GetTotal() > 0 {
		for _, row := range rsp.GetRows() {
			log.Infof("Query id: %v", row.AudioId)
			_, err := TranslateClient.Query(
				context.Background(),
				&pb.TQueryRequest{
					AudioId: row.AudioId,
				},
			)
			if err != nil {
				log.Errorf("Query error: %v", err)
			} else {
				log.Info("success")
			}
		}
	}

	return nil
}

func Trigger(AudioClient pb.AudioService, TranslateClient pb.TranslateService) error {
	Limit := int64(100)
	SortField := "audio_join_at"
	SortMode := int64(1)
	TranslateState := int64(pb.State_TranslateWaiting)
	rsp, err := AudioClient.Query(context.Background(), &pb.QueryRequest{
		Limit:                &Limit,
		SortField:            &SortField,
		SortMode:             &SortMode,
		FilterTranslateState: &TranslateState,
	})
	if err != nil {
		log.Errorf("Trigger error: %v", err)
		return err
	}

	log.Infof("Trigger total: %v", rsp.GetTotal())
	if rsp.GetTotal() > 0 {
		for _, row := range rsp.GetRows() {
			log.Infof("Trigger id: %v", row.AudioId)
			_, err = TranslateClient.Trigger(
				context.Background(),
				&pb.TriggerRequest{
					AudioId: row.AudioId,
				},
			)
			if err != nil {
				log.Errorf("Query error: %v", err)
			} else {
				log.Info("success")
			}
		}
	}

	return nil
}

func main() {
	// Create service
	srv := micro.NewService()
	srv.Init()

	var (
		err error
	)

	srvClient := srv.Client()
	err = srvClient.Init(
		client.RequestTimeout(time.Duration(10*time.Second)),
		client.DialTimeout(time.Duration(15*time.Second)),
	)
	if err != nil {
		return
	}

	// Create client
	AudioClient := pb.NewAudioService("task-service", srvClient)
	TranslateClient := pb.NewTranslateService("task-service", srvClient)

	for {
		log.Infof("---------- %s", time.Now())

		log.Info("Exec Query...")
		err = Query(AudioClient, TranslateClient)
		if err != nil {
			log.Error(err)
			time.Sleep(100 * time.Second)
			continue
		}

		log.Info("Trigger Query...")
		err = Trigger(AudioClient, TranslateClient)
		if err != nil {
			log.Error(err)
			time.Sleep(100 * time.Second)
			continue
		}

		time.Sleep(10 * time.Second)
	}
}
