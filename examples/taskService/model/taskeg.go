package model

import (
	"github.com/gin-gonic/gin"
	"github.com/go-basic/uuid"
	"log"
	"strconv"
	"taskService/db"
	"time"
)

type TaskegEntity struct {
	TaskId            	string     	`json:"taskId"`
	TaskName            string		`json:"taskName"`
	TaskType         	int     	`json:"taskType"`
}

// Taskeg 可以写一个自动生成结构体的工具
type Taskeg struct {
	TaskId            	string			`gorm:"primary_key" json:"taskId" form:"task_id"`
	TaskName            string			`json:"taskName" form:"task_name"`
	TaskType            int				`json:"taskType" form:"task_type"`
	CreateTime        	time.Time		`json:"createTime" form:"create_time"`
	UpdateTime     		time.Time     	`json:"updateTime" form:"update_time"`
}

// GetOne /*
func (t *Taskeg) GetOne(taskId string) (taskeg Taskeg, err error) {

	err = db.GormDb.Where("task_id = ?", taskId).First(&taskeg).Error

	if err != nil {
		log.Println(err)
		return taskeg, err
	}

	return taskeg, err
}

// Query /*
func (t *Taskeg) Query(c *gin.Context) (taskegs []Taskeg, total int, code int, msg string, err error) {

	page, _ := strconv.Atoi(c.Query("page"))
	pageSize, _ := strconv.Atoi(c.Query("limit"))

	gormDb := db.GormDb.Model(Taskeg{})

	// 如果有逻辑删除
	// gormDb = gormDb.Where("is_delete = 0")

	if keyword, isExist := c.GetQuery("keyword"); isExist == true {
		gormDb = gormDb.Where("task_name like ?", "%"+keyword+"%")
	}

	if taskType, isExist := c.GetQuery("taskType"); isExist == true {
		taskTypeInt, _ := strconv.Atoi(taskType)
		if taskTypeInt != 0 {
			gormDb = gormDb.Where("task_type = ?", taskTypeInt)
		}
	}

	if err := gormDb.Count(&total).Error; err != nil {
		log.Println(err)
	}

	if page > 0 && pageSize > 0 {
		gormDb = gormDb.Limit(pageSize).Offset((page - 1) * pageSize)
	}
	if err := gormDb.Find(&taskegs).Error; err != nil {
		log.Println(err)
	}

	return taskegs, total, 20000, "", err
}

// Add /*
func (t *Taskeg) Add(entity TaskegEntity) (taskeg Taskeg, code int, msg string, err error) {

	taskeg = Taskeg{
		TaskId: uuid.New(),
		TaskName: entity.TaskName,
		TaskType: entity.TaskType,
		CreateTime: time.Now(),
		UpdateTime: time.Now(),
	}
	err = db.GormDb.Create(&taskeg).Error

	if err != nil {
		return taskeg, 51101, "操作失败，请重试", err
	}

	return taskeg, 20000, "", err
}

// Update /*
func (t *Taskeg) Update(entity TaskegEntity) (taskeg Taskeg, code int, msg string, err error) {

	taskeg, err = t.GetOne(entity.TaskId)

	if err != nil {
		log.Println(err)
		return taskeg, 51201, "操作失败，请重试", err
	}

	// 更新
	{
		taskeg.TaskName = entity.TaskName
		taskeg.TaskType = entity.TaskType
		taskeg.UpdateTime = time.Now()
		err = db.GormDb.Model(&taskeg).Update(taskeg).Error

		if err != nil {
			log.Println(err)
			return taskeg, 51202, "操作失败，请重试", err
		}
	}

	return taskeg, 20000, "", err
}

// Delete /*
func (t *Taskeg) Delete(entity TaskegEntity) (code int, msg string, err error) {

	taskeg := Taskeg{
		TaskId: entity.TaskId,
	}

	// 实际情况下一般使用逻辑删除
	err = db.GormDb.Delete(&taskeg).Error

	if err != nil {
		log.Println(err)
		return 51201, "操作失败，请重试", err
	}

	return 20000, "", err
}