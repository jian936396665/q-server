package main

import (
	"context"
	"github.com/asim/go-micro/v3"
	"github.com/asim/go-micro/v3/logger"
	proto "taskService/proto"
)

const (
	TaskServiceName = "TaskService"
)

func main() {
  	service := micro.NewService()

	service.Init()

	taskClient := proto.NewTaskService(TaskServiceName, service.Client())

	// Make request
	rsp, err := taskClient.AddTask(context.Background(), &proto.AddRequest{
		TaskName: "John",
		TaskType: 1,
	})
	if err != nil {
		logger.Info(err)
		return
	}
	logger.Info(rsp)
}
