package db

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"taskService/config"
)

var GormDb *gorm.DB

func init() {

	connStr := fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/aiting_tme?parseTime=true&loc=Local",
		config.Cfg.MySQL.User,
		config.Cfg.MySQL.Password,
		config.Cfg.MySQL.Host,
		config.Cfg.MySQL.Port,
	)

	var err error
	GormDb, err = gorm.Open("mysql", connStr)

	if err != nil {
		panic(err)
	}
	//设置全局表名禁用复数
	GormDb.SingularTable(false)
}
