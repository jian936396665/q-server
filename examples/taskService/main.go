package main

import (
	_ "github.com/asim/go-micro/plugins/registry/kubernetes/v3"
	"github.com/asim/go-micro/v3"
	"github.com/asim/go-micro/v3/logger"
	"taskService/handler"
	proto "taskService/proto"
)

const (
	ServiceName = "TaskService"
)

func main() {
	// Create a new service. Optionally include some options here.
	service := micro.NewService(
		micro.Name(ServiceName),
		micro.Version("latest"),
		micro.Metadata(map[string]string{
			"type": "taskService",
		}),
	)

	// Init will parse the command line flags. Any flags set will
	// override the above settings. Options defined here will
	// override anything set on the command line.
	service.Init()

	// Register handler
	if err := proto.RegisterTaskServiceHandler(service.Server(), new(handler.TaskService)); err != nil {
		logger.Fatal(err)
	}

	// Run the server
	if err := service.Run(); err != nil {
		logger.Fatal(err)
	}
}
