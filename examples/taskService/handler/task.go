package handler

import (
	"context"
	. "taskService/model"
	"taskService/proto"
)

type TaskService struct{}

func (g *TaskService) AddTask(ctx context.Context, req *proto.AddRequest, rsp *proto.AddResponse) error {
	t := Taskeg{}
	taskeg, _, _, err := t.Add(TaskegEntity{
		TaskName: req.TaskName,
		TaskType: int(req.TaskType),
	})
	rsp.TaskId = taskeg.TaskId
	rsp.TaskName = taskeg.TaskName
	return err
}
