package main

import (
	"apiGateway/client"
	"apiGateway/router"
	"log"
)

func init() {
	client.RegisterService()
}

func main() {
	r := router.InitRouter()
	if err := r.Run(":" + client.Port); err != nil {
		log.Print(err.Error())
	}
}