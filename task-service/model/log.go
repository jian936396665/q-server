package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	pb "task-service/proto"
)

type LogCollection struct {
	Id        primitive.ObjectID `bson:"_id"`
	Category  pb.LogCategory     `bson:"category"`
	At        int64              `bson:"at"`
	Message   string             `bson:"message"`
	UserId    string             `bson:"user_id"`
	AudioId   string             `bson:"audio_id"`
	AudioName string             `bson:"audio_name"`
	Params    string             `bson:"params,omitempty"`
}
