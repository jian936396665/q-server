package model

type TranslateResult struct {
	Sentences string `bson:"sentences"`
	StartTime int64  `bson:"start_time"`
	EndTime   int64  `bson:"end_time"`
}
