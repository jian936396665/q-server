package model

type YTResponse struct {
	Rtn       int64  `json:"rtn"`
	Message   string `json:"message"`
	TaskId    string `json:"taskId"`
	RequestId string `json:"requestId"`
}

type YTCreateResponse struct {
	YTResponse
	AudioId string `json:"audioId"`
}

type YTQueryResponse struct {
	YTResponse
	Data YTQueryRespData `json:"data"`
}

type YTQueryRespData struct {
	StatusCode   int64          `json:"statusCode"`
	StatusText   string         `json:"statusText"`
	SpeechResult YTSpeechResult `json:"speechResult"`
}

type YTSpeechResult struct {
	ResultText string                 `json:"resultText"`
	Duration   int64                  `json:"duration"`
	Detail     []YTSpeechResultDetail `json:"detail"`
}

type YTSpeechResultDetail struct {
	Sentences string `json:"sentences"`
	StartTime string `json:"startTime"`
	EndTime   string `json:"endTime"`
}
