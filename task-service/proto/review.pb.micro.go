// Code generated by protoc-gen-micro. DO NOT EDIT.
// source: proto/review.proto

package taskservice

import (
	fmt "fmt"
	proto "google.golang.org/protobuf/proto"
	math "math"
)

import (
	context "context"
	api "github.com/asim/go-micro/v3/api"
	client "github.com/asim/go-micro/v3/client"
	server "github.com/asim/go-micro/v3/server"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// Reference imports to suppress errors if they are not otherwise used.
var _ api.Endpoint
var _ context.Context
var _ client.Option
var _ server.Option

// Api Endpoints for ReviewService service

func NewReviewServiceEndpoints() []*api.Endpoint {
	return []*api.Endpoint{}
}

// Client API for ReviewService service

type ReviewService interface {
	DispatchAppoint(ctx context.Context, in *DispatchAppointRequest, opts ...client.CallOption) (*DispatchAppointResponse, error)
}

type reviewService struct {
	c    client.Client
	name string
}

func NewReviewService(name string, c client.Client) ReviewService {
	return &reviewService{
		c:    c,
		name: name,
	}
}

func (c *reviewService) DispatchAppoint(ctx context.Context, in *DispatchAppointRequest, opts ...client.CallOption) (*DispatchAppointResponse, error) {
	req := c.c.NewRequest(c.name, "ReviewService.DispatchAppoint", in)
	out := new(DispatchAppointResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for ReviewService service

type ReviewServiceHandler interface {
	DispatchAppoint(context.Context, *DispatchAppointRequest, *DispatchAppointResponse) error
}

func RegisterReviewServiceHandler(s server.Server, hdlr ReviewServiceHandler, opts ...server.HandlerOption) error {
	type reviewService interface {
		DispatchAppoint(ctx context.Context, in *DispatchAppointRequest, out *DispatchAppointResponse) error
	}
	type ReviewService struct {
		reviewService
	}
	h := &reviewServiceHandler{hdlr}
	return s.Handle(s.NewHandler(&ReviewService{h}, opts...))
}

type reviewServiceHandler struct {
	ReviewServiceHandler
}

func (h *reviewServiceHandler) DispatchAppoint(ctx context.Context, in *DispatchAppointRequest, out *DispatchAppointResponse) error {
	return h.ReviewServiceHandler.DispatchAppoint(ctx, in, out)
}
