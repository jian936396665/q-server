module task-service

go 1.16

require (
	github.com/asim/go-micro/plugins/registry/kubernetes/v3 v3.0.0-20211006165514-a99a1e935651
	github.com/asim/go-micro/v3 v3.5.2
	go.mongodb.org/mongo-driver v1.7.2
	google.golang.org/protobuf v1.26.0
)

// This can be removed once etcd becomes go gettable, version 3.4 and 3.5 is not,
// see https://github.com/etcd-io/etcd/issues/11154 and https://github.com/etcd-io/etcd/issues/11931.
replace google.golang.org/grpc => google.golang.org/grpc v1.26.0
