package handler

import (
	"context"
	"task-service/model"
	pb "task-service/proto"
	"time"

	log "github.com/asim/go-micro/v3/logger"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type AudioService struct {
	Store *mongo.Collection
}

func (e *AudioService) Create(ctx context.Context, req *pb.CreateRequest, rsp *pb.CreateResponse) error {
	log.Debugf("AudioService.Create request: %v", req)

	audioId := req.GetAudioId()
	if audioId == "" {
		audioId = primitive.NewObjectID().Hex()
	}

	objectID, err := primitive.ObjectIDFromHex(audioId)
	if err != nil {
		log.Errorf("AudioService.Create error: %v", err)
		return err
	}

	audioCreatedAt := req.GetAudioCreatedAt()
	if audioCreatedAt == 0 {
		audioCreatedAt = time.Now().Unix()
		req.AudioCreatedAt = &audioCreatedAt
	}

	data := &model.TaskCollection{
		AudioId:         objectID,
		AudioName:       req.GetAudioName(),
		AudioUri:        req.GetAudioUri(),
		AudioLength:     req.GetAudioLength(),
		AudioCreatedAt:  req.GetAudioCreatedAt(),
		AudioPriority:   req.GetAudioPriority(),
		AudioState:      req.GetAudioState(),
		TranslateJoinAt: req.GetTranslateJoinAt(),
		TranslateState:  model.TranslateStateWaiting,
	}
	log.Debugf("AudioService.Create data: %+v", data)
	result, err := e.Store.InsertOne(ctx, data)
	if err != nil {
		log.Errorf("AudioService.Create error: %v", err)
		return err
	}
	InsertedID := result.InsertedID.(primitive.ObjectID).Hex()
	rsp.AudioId = InsertedID

	log.Debugf("AudioService.Create response: %v", rsp)
	return nil
}

func (e *AudioService) Query(ctx context.Context, req *pb.QueryRequest, rsp *pb.QueryResponse) error {
	log.Debugf("AudioService.Query request: %v", req)

	limit := req.GetLimit()
	if limit == 0 {
		limit = 30
	}
	rsp.Limit = limit

	page := req.GetPage()
	if page < 1 {
		page = 1
	}
	rsp.Page = page

	sortField := req.GetSortField()
	if sortField == "" {
		sortField = "audio_created_at"
	}

	sortMode := req.GetSortMode()
	if sortMode == 0 {
		sortMode = -1
	}

	filter := bson.D{}

	filterName := req.GetFilterName()
	if filterName != "" {
		filter = append(filter, bson.E{Key: "audio_name", Value: bson.M{"$regex": filterName}})
	}

	filterTranslateState := req.GetFilterTranslateState()
	if filterTranslateState != 0 {
		filter = append(filter, bson.E{Key: "translate_state", Value: filterTranslateState})
	}

	filterTranslateStates := req.GetFilterTranslateStates()
	if len(filterTranslateStates) != 0 {
		filter = append(filter, bson.E{
			Key: "translate_state",
			Value: bson.M{"$in": filterTranslateStates},
		})
	}

	filterAudioState := req.GetFilterAudioState()
	if filterAudioState != 0 {
		filter = append(filter, bson.E{
			Key: "audio_state",
			Value: filterAudioState,
		})
	}

	filterAudioStates := req.GetFilterAudioStates()
	if len(filterAudioStates) != 0 {
		filter = append(filter, bson.E{
			Key: "audio_state",
			Value: bson.M{"$in": filterAudioStates},
		})
	}

	filterCreatedAt := req.GetFilterCreatedAt()
	if filterCreatedAt != nil {
		filter = append(filter, bson.E{Key: "audio_created_at", Value: bson.M{
			"$gt": filterCreatedAt.Start,
			"$lt": filterCreatedAt.End,
		}})
	}

	filterJoinAt := req.GetFilterJoinAt()
	if filterJoinAt != nil {
		filter = append(filter, bson.E{Key: "translate_join_at", Value: bson.M{
			"$gt": filterJoinAt.Start,
			"$lt": filterJoinAt.End,
		}})
	}

	if req.FilterReleaseState != nil {
		filter = append(filter, bson.E{
			Key:   "release_state",
			Value: req.GetFilterReleaseState(),
		})
	}

	if req.FilterDispatchBy != nil {
		filterDispatchBy := req.GetFilterDispatchBy()
		if filterDispatchBy == "" {
			filter = append(filter, bson.E{Key: "dispatch_by", Value: bson.M{"$exists": false}})
		} else {
			filter = append(filter, bson.E{Key: "dispatch_by", Value: filterDispatchBy})
		}
	}

	total, err := e.Store.CountDocuments(ctx, filter)
	if err != nil {
		log.Errorf("AudioService.Query error: %v", err)
		return err
	}
	rsp.Total = total

	cursor, err := e.Store.Find(
		ctx,
		filter,
		options.Find().SetSkip((page-1)*limit),
		options.Find().SetLimit(limit),
		options.Find().SetSort(bson.M{sortField: sortMode}),
	)
	if err != nil {
		log.Errorf("AudioService.Query error: %v", err)
		return err
	}

	defer func(cursor *mongo.Cursor, ctx context.Context) {
		_ = cursor.Close(ctx)
	}(cursor, ctx)

	for cursor.Next(context.Background()) {
		result := model.TaskCollection{}
		err := cursor.Decode(&result)
		if err != nil {
			log.Errorf("MongoDB cursor close error: %v", err)
			return err
		}
		rsp.Rows = append(rsp.Rows, &pb.QueryRowResponse{
			AudioId:              result.AudioId.Hex(),
			AudioName:            result.AudioName,
			AudioLength:          result.AudioLength,
			AudioCreatedAt:       result.AudioCreatedAt,
			AudioCreatedBy:       result.AudioCreatedBy,
			AudioState:           result.AudioState,
			TranslateJoinAt:      result.TranslateJoinAt,
			TranslateJoinBy:      result.TranslateJoinBy,
			TranslateCompletedAt: result.TranslateCompletedAt,
			TranslateState:       result.TranslateState,
			AudioTotalWords:      result.AutoTotalWords,
			DispatchAt:           result.DispatchAt,
			DispatchBy:           result.DispatchBy,
			SnapshotAt:           result.SnapshotAt,
			ReleaseAt:            result.ReleaseAt,
			ReleaseState:         result.ReleaseState,
		})
	}
	if err := cursor.Err(); err != nil {
		return err
	}

	log.Debugf("AudioService.Query response: %v", rsp)
	return nil
}

func (e *AudioService) Detail(ctx context.Context, req *pb.DetailRequest, rsp *pb.DetailResponse) error {
	log.Debugf("AudioService.Detail request: %v", req)

	audioId := req.GetAudioId()
	objectID, err := primitive.ObjectIDFromHex(audioId)
	if err != nil {
		log.Errorf("AudioService.Detail error: %v", err)
		return err
	}

	result := model.TaskCollection{}
	err = e.Store.FindOne(ctx, bson.M{"_id": objectID}).Decode(&result)
	if err != nil {
		log.Errorf("AudioService.Detail error: %v", err)
		return err
	}

	rsp.AudioId = result.AudioId.Hex()
	rsp.AudioName = result.AudioName
	rsp.AudioUri = result.AudioUri
	rsp.AudioLength = result.AudioLength
	rsp.AudioTotalWords = result.AutoTotalWords
	rsp.AudioCreatedAt = result.AudioCreatedAt
	rsp.AudioCreatedBy = result.AudioCreatedBy
	rsp.AudioState = result.AudioState
	rsp.TranslateJoinAt = result.TranslateJoinAt
	rsp.TranslateJoinBy = result.TranslateJoinBy
	rsp.TranslateCompletedAt = result.TranslateCompletedAt
	rsp.TranslateTaskId = result.TranslateTaskId
	rsp.TranslateState = result.TranslateState

	var rows []*pb.TranslateResult
	for key, value := range result.TranslateResult {
		var row pb.TranslateResult
		row.Sentences = value.Sentences
		row.StartTime = value.StartTime
		row.EndTime = value.EndTime
		row.Key = int64(key + 1)
		rows = append(rows, &row)
	}

	rsp.TranslateResult = rows

	var snapshotRows []*pb.TranslateResult
	for key, value := range result.SnapshotResult {
		var row pb.TranslateResult
		row.Sentences = value.Sentences
		row.StartTime = value.StartTime
		row.EndTime = value.EndTime
		row.Key = int64(key + 1)
		snapshotRows = append(snapshotRows, &row)
	}
	rsp.SnapshotResult = snapshotRows

	log.Debugf("AudioService.Detail response: %v", rsp)
	return nil
}

func (e *AudioService) Delete(ctx context.Context, req *pb.DeleteRequest, rsp *pb.DeleteResponse) error {
	log.Debugf("AudioService.Delete request: %v", req)

	audioId := req.GetAudioId()
	objectID, err := primitive.ObjectIDFromHex(audioId)
	if err != nil {
		log.Errorf("AudioService.Delete error: %v", err)
		return err
	}

	_, err = e.Store.DeleteOne(ctx, bson.M{"_id": objectID})
	if err != nil {
		log.Errorf("AudioService.Delete error: %v", err)
		return err
	}

	log.Debugf("AudioService.Delete response: %v", rsp)
	return nil
}
