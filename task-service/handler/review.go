package handler

import (
	"context"
	"errors"
	log "github.com/asim/go-micro/v3/logger"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"task-service/model"
	pb "task-service/proto"
	"time"
)

type ReviewService struct {
	StoreTask *mongo.Collection
}

func (e *ReviewService) DispatchAppoint(
	ctx context.Context,
	req *pb.DispatchAppointRequest,
	rsp *pb.DispatchAppointResponse,
) error {
	log.Debugf("DispatchAppoint request: %v", req)

	aId := req.GetAudioId()
	aOId, err := primitive.ObjectIDFromHex(aId)
	if err != nil {
		log.Errorf("DispatchAppoint error: %v", err)
		return err
	}

	var tRow model.TaskCollection
	err = e.StoreTask.FindOne(context.Background(), bson.M{"_id": aOId}).Decode(&tRow)
	if err != nil {
		log.Errorf("DispatchAppoint error: %v", err)
		return err
	}

	//状态校验
	if tRow.TranslateState != model.TranslateStateCompleted {
		return errors.New("音频转换尚未完成")
	}

	//分派校验
	if tRow.DispatchBy != "" {
		return errors.New("审核任务已经派单")
	}

	//存储
	data := bson.M{"$set": bson.M{
		"dispatch_by": req.GetDispatchBy(),
		"dispatch_at": time.Now().Unix(),
	}}
	_, err = e.StoreTask.UpdateByID(context.Background(), aOId, data)
	if err != nil {
		log.Errorf("DispatchAppoint error: %v", err)
		return err
	}

	log.Debugf("DispatchAppoint response: %v", rsp)
	return nil
}
